#!/usr/bin/env python3
"""This file is part of Toiler.

Toiler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Toiler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Toiler.  If not, see <https://www.gnu.org/licenses/>.
"""
import asyncio
import json
import logging
import uuid

from io import BytesIO

from aiohttp import web

from logs import configure_logging, AccessLogger

configure_logging()
log = logging.getLogger(__name__)
routes = web.RouteTableDef()


def is_valid_uuid(value):
    try:
        uuid.UUID(str(value))
        return True
    except ValueError:
        return False


async def read_stderr(process, queue):
    async for line in process.stderr:
        queue.put_nowait(line)


async def run(cmd, working_dir, queue):
    process = await asyncio.create_subprocess_shell(
        cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE,
        cwd=working_dir, limit=2**24)  # 16Mb buffer limit when using pipes
    asyncio.create_task(read_stderr(process, queue))
    async for line in process.stdout:
        queue.put_nowait(line)
    await asyncio.wait_for(process.wait(), timeout=0.1)
    return process.returncode


@routes.post('/tasks')
async def create_task(request):
    try:
        body = await request.json()
    except ValueError as e:
        log.error(f'Could not parse json: {e}')
        return web.Response(status=400, text='Could not parse json')
    if body.get('cmd') is None or body.get('wd') is None:
        log.error(f'Invalid payload, received: {json.dumps(body)}')
        return web.Response(status=400, text='Invalid payload')
    queue = asyncio.Queue()
    future = asyncio.create_task(run(body['cmd'], body['wd'], queue))
    task_id = str(uuid.uuid4())
    request.app['tasks'].update({task_id: {'future': future, 'queue': queue}})
    log.info(f'Created task {task_id}.')
    return web.Response(status=201, text=task_id)


@routes.get('/tasks')
async def list_tasks(request):
    return web.Response(
        status=200, text=json.dumps(list(request.app['tasks'].keys())))


@routes.get('/tasks/{task_id}')
async def poll_task(request):
    task_id = request.match_info['task_id']
    if not is_valid_uuid(task_id):
        return web.Response(status=400, text='Invalid uuid.')
    task = request.app['tasks'].get(task_id)
    if task is None:
        return web.Response(status=404, text='Task not found.')
    future = task['future']
    queue = task['queue']
    output = BytesIO()
    try:
        while True:
            output.write(queue.get_nowait())
    except asyncio.QueueEmpty:
        pass
    output = output.getvalue().decode('utf-8')
    if future.done():
        request.app['tasks'].pop(task_id, None)
        returncode = future.result()
        if returncode != 0:
            msg = (
                f'Task {task_id} finished with exit status {returncode}.')
            log.warning(msg)
            return web.Response(status=418, text=f'{output}\n{msg}')
        log.info(f'Task {task_id} finished.')
    return web.Response(status=200, text=output.strip())


@routes.get('/tasks/{task_id}/queue')
async def get_queue_size(request):
    task_id = request.match_info['task_id']
    task = request.app['tasks'].get(task_id)
    if task is None:
        return web.Response(status=404, text='')
    return web.Response(status=200, text=str(task['queue'].qsize()))


@routes.get('/')
async def health(_):
    return web.Response(status=200, text='')


def create_application():
    app = web.Application()
    app.add_routes(routes)
    app['tasks'] = {}
    return app


if __name__ == '__main__':
    web.run_app(create_application(), access_log_class=AccessLogger)
