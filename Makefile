all:
	@echo "make clean"

clean:
	find . -path '*/htmlcov*' -delete
	find . -path '*/__pycache__*' -delete
	find . -path '*/.coverage' -delete
	find . -path '*/.pytest_cache*' -delete

test: clean
	pytest \
		-vv \
		--cov-config=.coveragerc \
		--cov-report=html \
		--cov-report=term \
		--cov toiler \
		--cov logs

lint: clean
	find . -name '*.py' | xargs pylint || true
