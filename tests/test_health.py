"""This file is part of Toiler.

Toiler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Toiler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Toiler.  If not, see <https://www.gnu.org/licenses/>.
"""


async def test_health(client, caplog):
    resp = await client.get('/')
    assert resp.status == 200
    text = await resp.text()
    assert text == ''
    assert len(caplog.records) == 1
    log_msg = iter([log.getMessage() for log in caplog.records])
    assert 'GET / 200' in next(log_msg)
