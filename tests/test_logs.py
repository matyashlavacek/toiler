"""This file is part of Toiler.

Toiler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Toiler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Toiler.  If not, see <https://www.gnu.org/licenses/>.
"""
import logging

import logs


def clean_logging():
    while len(logging.root.handlers) != 0:
        logging.root.handlers.pop()


def mock_create_handler(handler_list, handler):

    def create_hander():
        handler_list.append(True)
        return handler
    return create_hander


def test_logging_configure_once(monkeypatch):
    clean_logging()
    handler = logging.StreamHandler()
    str_list = []
    monkeypatch.setattr(
        logs, 'create_stream_handler', mock_create_handler(str_list, handler))
    file_list = []
    monkeypatch.setattr(
        logs, 'create_file_handler', mock_create_handler(file_list, handler))
    logs.configure_logging()
    logs.configure_logging()
    assert len(str_list) == 2
    assert len(file_list) == 2


def test_logging_already_has_stream_handler(monkeypatch):
    clean_logging()
    str_hdlr = logging.StreamHandler()
    str_hdlr.set_name('stream_handler')
    file_hldr = logging.StreamHandler()
    str_list = []
    monkeypatch.setattr(
        logs, 'create_stream_handler', mock_create_handler(str_list, str_hdlr))
    file_list = []
    monkeypatch.setattr(
        logs, 'create_file_handler', mock_create_handler(file_list, file_hldr))
    logs.configure_logging()
    logs.configure_logging()
    assert len(str_list) == 1
    assert len(file_list) == 1


def test_logging_already_has_file_handler(monkeypatch):
    clean_logging()
    str_hdlr = logging.StreamHandler()
    file_hldr = logging.StreamHandler()
    file_hldr.set_name('file_handler')
    str_list = []
    monkeypatch.setattr(
        logs, 'create_stream_handler', mock_create_handler(str_list, str_hdlr))
    file_list = []
    monkeypatch.setattr(
        logs, 'create_file_handler', mock_create_handler(file_list, file_hldr))
    logs.configure_logging()
    logs.configure_logging()
    assert len(str_list) == 1
    assert len(file_list) == 1
