"""This file is part of Toiler.

Toiler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Toiler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Toiler.  If not, see <https://www.gnu.org/licenses/>.
"""
import asyncio
import json

from aiohttp import web

import toiler

from conftest import load_module


def test___main___(monkeypatch):
    called = []
    def mock_run_app(app, access_log_class=None):
        called.append((app, access_log_class))
    monkeypatch.setattr(web, 'run_app', mock_run_app)
    load_module('__main__', './toiler.py')
    assert len(called) == 1


async def test_read_stderr():
    async def mock_stderr():
        yield 'text'
    class mock_proc:
        stderr = mock_stderr()
    proc = mock_proc()
    queue = asyncio.Queue()
    await toiler.read_stderr(proc, queue)
    assert queue.get_nowait() == 'text'
    assert queue.qsize() == 0


async def test_create_task(monkeypatch, client, caplog):
    payload = {'cmd': 'echo "hello"', 'wd': '.'}

    async def mocked_run(_, __, queue):
        queue.put_nowait('hello'.encode('utf-8'))
        return 0
    monkeypatch.setattr(toiler, 'run', mocked_run)
    resp = await client.post('/tasks', json=payload)
    assert resp.status == 201
    uuid = await resp.text()
    resp = await client.get(f'/tasks/{uuid}')
    assert resp.status == 200
    text = await resp.text()
    assert text == 'hello'
    resp = await client.get(f'/tasks/{uuid}')
    assert resp.status == 404
    text = await resp.text()
    assert text == 'Task not found.'
    assert len(caplog.records) == 5
    log_msg = iter([log.getMessage() for log in caplog.records])
    assert next(log_msg) == f'Created task {uuid}.'
    assert 'POST /tasks 201' in next(log_msg)
    assert next(log_msg) == f'Task {uuid} finished.'
    assert f'GET /tasks/{uuid} 200' in next(log_msg)
    assert f'GET /tasks/{uuid} 404' in next(log_msg)


async def test_create_task_fails_invalid_json(client, caplog):
    payload = 'garbage'
    resp = await client.post('/tasks', data=payload)
    assert resp.status == 400
    body = await resp.text()
    assert body == 'Could not parse json'
    assert len(caplog.records) == 2
    log_msg = iter([log.getMessage() for log in caplog.records])
    assert next(log_msg) == (
        'Could not parse json: Expecting value: line 1 column 1 (char 0)')
    assert 'POST /tasks 400 180B' in next(log_msg)


async def test_create_task_fails_invalid_payload(client, caplog):
    payload = {'cmd': 'echo "hello"'}
    resp = await client.post('/tasks', json=payload)
    assert resp.status == 400
    body = await resp.text()
    assert body == 'Invalid payload'
    assert len(caplog.records) == 2
    log_msg = iter([log.getMessage() for log in caplog.records])
    assert next(log_msg) == f'Invalid payload, received: {json.dumps(payload)}'
    assert 'POST /tasks 400 175B' in next(log_msg)


async def test_list_empty_tasks(client, caplog):
    resp = await client.get('/tasks')
    assert resp.status == 200
    text = await resp.text()
    assert text == '[]'
    assert len(caplog.records) == 1
    log_msg = iter([log.getMessage() for log in caplog.records])
    assert 'GET /tasks 200 152B' in next(log_msg)


async def test_get_tasks_invalid_uuid(client, caplog):
    resp = await client.get('/tasks/non-uuid')
    assert resp.status == 400
    text = await resp.text()
    assert text == 'Invalid uuid.'
    assert len(caplog.records) == 1
    log_msg = iter([log.getMessage() for log in caplog.records])
    assert 'GET /tasks/non-uuid 400' in next(log_msg)


async def test_get_tasks_not_found(client, caplog):
    uuid = '99999999-9999-9999-9999-999999999999'
    resp = await client.get(f'/tasks/{uuid}')
    assert resp.status == 404
    text = await resp.text()
    assert text == 'Task not found.'
    assert len(caplog.records) == 1
    log_msg = iter([log.getMessage() for log in caplog.records])
    assert f'GET /tasks/{uuid} 404' in next(log_msg)


async def test_get_queue_size(monkeypatch, client, caplog):
    payload = {'cmd': 'echo "hello"', 'wd': '.'}

    async def mocked_run(_, __, queue):
        queue.put_nowait('hello'.encode('utf-8'))
        return 0
    monkeypatch.setattr(toiler, 'run', mocked_run)
    resp = await client.post('/tasks', json=payload)
    assert resp.status == 201
    uuid = await resp.text()
    resp = await client.get(f'/tasks/{uuid}/queue')
    assert resp.status == 200
    text = await resp.text()
    assert text == '1'
    assert len(caplog.records) == 3
    log_msg = iter([log.getMessage() for log in caplog.records])
    assert next(log_msg) == f'Created task {uuid}.'
    assert 'POST /tasks 201' in next(log_msg)
    assert f'GET /tasks/{uuid}/queue 200' in next(log_msg)


async def test_get_queue_size_invalid_uuid(client, caplog):
    resp = await client.get('/tasks/non-uuid/queue')
    assert resp.status == 404
    text = await resp.text()
    assert text == ''
    assert len(caplog.records) == 1
    log_msg = iter([log.getMessage() for log in caplog.records])
    assert 'GET /tasks/non-uuid/queue 404' in next(log_msg)
