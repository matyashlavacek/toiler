"""This file is part of Toiler.

Toiler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Toiler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Toiler.  If not, see <https://www.gnu.org/licenses/>.
"""
import logging

from aiohttp.abc import AbstractAccessLogger


def configure_logging():
    logging.root.setLevel(logging.INFO)
    for handler in logging.root.handlers:
        if handler.get_name() == 'stream_handler':
            return  # handler already configured
        if handler.get_name() == 'file_handler':
            return  # handler already configured
    logging.root.addHandler(create_stream_handler())
    logging.root.addHandler(create_file_handler())


def create_stream_handler():
    stream_handler = logging.StreamHandler()
    stream_handler.set_name('stream_handler')
    stream_handler.setFormatter(create_formatter())
    return stream_handler


def create_file_handler():
    file_handler = logging.FileHandler('info.log')
    file_handler.set_name('file_handler')
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(create_formatter())
    return file_handler


def create_formatter():
    fmt = '%(asctime)s: %(levelname)s: %(filename)s: %(message)s'
    return logging.Formatter(fmt=fmt, datefmt='%Y-%m-%d %H:%M:%S %z')


class AccessLogger(AbstractAccessLogger):

    def log(self, request, response, time):
        logging.info(
            f'{request.method} {request.path} {response.status} '
            f'{response.body_length}B {round(time * 1000)}ms')
