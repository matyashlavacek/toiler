# Toiler

[![pipeline status](https://gitlab.com/matyashlavacek/toiler/badges/master/pipeline.svg)](https://gitlab.com/matyashlavacek/toiler/commits/master)
[![coverage report](https://gitlab.com/matyashlavacek/toiler/badges/master/coverage.svg)](https://gitlab.com/matyashlavacek/toiler/commits/master)

## Setup

### Production:

```
pip install -r requirements.txt
```

### Development:

```
pip install -r requirements-dev.txt
```

## Run

```
./toiler.py
```

## Test & Coverage

### Run tests

```
make test
```

### View coverage in browser

```
open htmlcov/index.html
```

## Lint

```
make lint
```

## Clean

```
make clean
```

### Notes

- toiler - _​a person who works very hard and/or for a long time, usually doing hard physical work_