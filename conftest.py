"""This file is part of Toiler.

Toiler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Toiler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Toiler.  If not, see <https://www.gnu.org/licenses/>.
"""
from importlib.machinery import SourceFileLoader
from importlib.util import module_from_spec, spec_from_loader

import pytest

from logs import AccessLogger
from toiler import create_application


@pytest.fixture
async def client(aiohttp_server, aiohttp_client):
    server = await aiohttp_server(
        create_application(), access_log_class=AccessLogger)
    return await aiohttp_client(server)


def load_module(fullname, path):
    loader = SourceFileLoader(fullname, path)
    spec = spec_from_loader(loader.name, loader)
    module = module_from_spec(spec)
    loader.exec_module(module)
    return module
